# 1.3.2

* Foundry 11 compatibility

# 1.3.1

* Foundry 10 compatibility

# 1.3.0

* Remove semver build metadata from version so Foundry version comparison doesn't shit itself
* No changes otherwise at all

# 1.2.1+0.8.8

* Fix permission level not working for the players list UI ([#9](https://gitlab.com/foundry-azzurite/cursor-hider/-/issues/9))

# 1.2.0+0.8.6

* Players list icon changes ([#3](https://gitlab.com/foundry-azzurite/cursor-hider/-/issues/3), [#4](https://gitlab.com/foundry-azzurite/cursor-hider/-/issues/4))
    * Hide the icon by default
    * Make the icon less bright 
    * Show the icon also when the cursor is shown, not only when it is hidden
    * Make icon clickable to hide/show the cursor ([#5](https://gitlab.com/foundry-azzurite/cursor-hider/-/issues/5))
    * Add option to show the icon always, even while not hovering over the players list
* Add `toggleCursor` macro API method

# 1.1.8+0.8.6

* Compatibility with 0.8.6
* Started using libWrapper + settings-extender module dependencies
* Simple API for modules/macros

# 1.1.7

* Compatibility with 0.6.1

# 1.1.6

* Verify & update core 0.6.0 compatibility

# 1.1.5

* Compatibility with 0.5.4

# 1.1.4

* Update to latest version of settings-extender

# 1.1.3

* Compatibility with FoundryVTT 0.4.4

# 1.1.2

* Compatibility with FoundryVTT 0.4.3

# 1.1.1

* Update dependency

# 1.1.0

* Add hover text to "cursor hidden" image in the player list 
