export default {
	MODULE_NAME: 'cursor-hider',
	MINIMUM_PERMISSION: 'minimumPermission',
	LAST_VERSION: 'lastVersion'
};
